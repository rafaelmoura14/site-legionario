// @ts-nocheck
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'um site legionario 7 fev 2023',
  tagline: 'um site legionario',
  favicon: 'img/templar.ico',

  // Set the production url of your site here
  url: 'https://rafaelmoura14.gitlab.io',
  // Set the /<baseUrl2222:2:/..>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  //baseUrl:"/",
  baseUrl: process.env.BASE_URL || "/",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'rafaelmoura14', // Usually your GitHub org/user name.
  projectName: 'rafaelmoura14.gitlab.io', // Usually your repo name.
  deploymentBranch: "main", // para github pages


  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // // Please change this to your repo.
          // // Remove this to remove the "edit this spage" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  plugins: [
    [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      {
        language: "en",
      }
    ],
  ],


  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'legionário',
        logo: {
          alt: 'My Site Logo',
          src: 'img/templar.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'About me',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://github.com/leg1on4rio',
            label: 'GitHub',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Projetos',
            items: [
              {
                label: 'GitHub',
                href: 'https://github.com/leg1on4rio',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/leg1on4rio',
              },
              // {
              //   label: 'Tutorial',
              //   to: '/docs/intro',
              // },
            ],
          },
          {
            title: 'Social',
            items: [
              {
                label: 'Twitter',
                href: 'https://twitter.com/leg1on4rio',
              },
              {
                label: 'Instagram',
                href: 'https://www.instagram.com/leg1on4rio/',
              },
              
             
            ],
          },
          {
            title: 'Outras coisas muito legais',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
            
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} site@leg1on4rio, Inc. Built with Docusaurus.`,
      },
      
      colorMode:{
      defaultMode:'dark',

      },

      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
